class ApplicationRecord < ActiveRecord::Base

  validates :name, presence: true

  has_attached_file :picture, styles: { medium: "350x350>", thumb: "100x100>", small: "35x35>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :picture, content_type: /\Aimage\/.*\z/

  self.abstract_class = true
end
